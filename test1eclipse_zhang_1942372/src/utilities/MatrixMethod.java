/**
 * Guang Kun Zhang
 * 1942372
 * Test1 Java310 Question2 
 */

package utilities;

import org.junit.Assert;

/**
 * @author guang
 * {@link https://gitlab.com/guang-zh/test1_zhang_1942372.git}
 */


public class MatrixMethod {
	
	// Duplicate() method for duplicating matrix Return int[][]
	public static int[][] duplicate(int[][] inputMatrix) {
		
		// Create outputMatrix and define its size
		int[][] outputMatrix = new int[inputMatrix.length][];
		for (int i=0; i<outputMatrix.length; i++) {
			outputMatrix[i] = new int[inputMatrix[i].length*2];
		}
		
		// Loop through outputMatrix and fill with duplicate inputMatrix 
		for (int i=0; i<outputMatrix.length; i++) {
			int index=0;
			for (int j=0; j<outputMatrix[i].length; j++) {
				outputMatrix[i][j]=inputMatrix[i][index];
				index++;
				if (index==inputMatrix[i].length) {
					index=0;
				}
			}
		}
		return outputMatrix;
	}
}
