/**
 * Guang Kun Zhang
 * 1942372
 * Test1 Java310 Question2 tests
 */

package tests;

import utilities.MatrixMethod;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.junit.Assert;

/**
 * @author guang
 * {@link https://gitlab.com/guang-zh/test1_zhang_1942372.git}
 */


public class MatrixMethodTests {
	
	// Test duplicate() method
	@Test
	public void testDuplicate() {
		int[][] matrix = {{1,7,8},{9,10,16}};
		int[][] result = MatrixMethod.duplicate(matrix);
		int[][] expected = {{1,7,8,1,7,8},{9,10,16,9,10,16}};
		for (int i=0; i<result.length; i++) {
			for (int j=0; j<result[i].length; j++) {
				Assert.assertEquals(expected[i][j], result[i][j]);
			}
		}
	}
	
	// Test duplicate() method by comparing Arrays of various sizes
	@Test
	public void testDuplicateArrays() {
		int[][] matrix = {{-1,8},{9,0},{2,3,4},{-10,30}};
		int[][] result = MatrixMethod.duplicate(matrix);
		int[][] expected = {{-1,8,-1,8},{9,0,9,0},{2,3,4,2,3,4},{-10,30,-10,30}};
		for (int i=0; i<result.length; i++) {
			Assert.assertArrayEquals(expected[i], result[i]);
		}
	}
}
