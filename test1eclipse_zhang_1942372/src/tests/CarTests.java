/**
 * Guang Kun Zhang
 * 1942372
 * Test1 Java310 Tests for Car.java within package vehicles 
 */

package tests;

import vehicles.Car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * @author guang
 * {@link https://gitlab.com/guang-zh/test1_zhang_1942372.git}
 */

// Tests for methods in Car.java within package vehicles
public class CarTests {
	// Test constructor for creating Car object with positive speed and type
	@Test
	public void testCar() {
		Car newCar = new Car(6);
	}
	// Test Car() for wrong type
	@Test
	public void testCarinputType() {
		Car newCar = new Car(5.99);
	}
	// Test Car() for negative speed integer
	@Test
	public void testCarNegativeSpeed() {
		Car newCar = new Car(-6);
	}
	
	// Test getSpeed() method
	@Test
	public void testGetSpeed() {
		Car newCar = new Car(8);
		int expectedSpeed = 8;
		assertEquals(expectedSpeed, newCar.getSpeed());
	}
	
	// Test getLocation() method
	@Test
	public void testGetLocation() {
		Car newCar = new Car(10);
		int expectedLocation = 0;
		assertEquals(expectedLocation, newCar.getLocation());
	}
	
	// Test moveRight() method
	@Test
	public void testMoveRight() {
		Car newCar = new Car(8);
		newCar.moveRight();
		int location = newCar.getLocation();
		int expectedLocation = 8;
		assertEquals(expectedLocation, location);
	}
	
	// Test moveLeft() method
	@Test
	public void testMoveLeft() {
		Car newCar = new Car(9);
		newCar.moveLeft();
		int location = newCar.getLocation();
		int expectedLocation = -9;
		assertEquals(expectedLocation, location);
	}
	
	// Test accelerate() method
	@Test
	public void testAccelerate() {
		Car newCar = new Car(5);
		newCar.accelerate();
		int newSpeed = newCar.getSpeed();
		int expectedSpeed = 6;
		assertEquals(expectedSpeed, newSpeed);
	}
	
	// Test decelerate() method
	@Test
	public void testDecelerate() {
		Car newCar = new Car(9);
		newCar.decelerate();
		int newSpeed = newCar.getSpeed();
		int expectedSpeed = 8;
		assertEquals(expectedSpeed, newSpeed);
	}
	
	// Test decelerate() method for zero speed
	@Test
	public void testDecelerateZeroSpeed() {
		Car newCar = new Car(0);
		newCar.decelerate();
		int newSpeed = newCar.getSpeed();
		int expectedSpeed = 0;
		assertEquals(expectedSpeed, newSpeed);
	}

}
